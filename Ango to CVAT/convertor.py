import json
import xml.etree.ElementTree as ET
import sys
import argparse

def points_to_string(points):
    point_string = ''
    for point in points:
        point_string = point_string + str( round(point[0], 2) )  + ',' + str(round(point[1], 2)) + ';'
    return point_string[:-1]

def read_json(file_name = 'Fisheye Project 4.json'):
    with open(file_name) as f:
     data = json.load(f)
    return data

def append_xml(root, image_name, label, occluded, source, points, z_order):
    image = ET.SubElement(root, 'image', name = image_name)
    polygon = ET.SubElement(image, 'polygon', label=label, occluded=occluded, source=source, points=points, z_order = z_order)

def get_image_details(image_name):
    cuts = image_name.split("/")
    return cuts[-1], cuts[-2]

def get_content_from_tasks(tasks):
    if(type(tasks[0]['content']) is dict):
        return (tasks[0]['content'])
    else:
        return (tasks[1]['content']) 

def process_data(data, roots, total_items = 0):
    if total_items <= 0:
        total_items = len(data) + 1
    for item in data[:total_items]:
        image_name, image_class = get_image_details(item['asset'])
        #print(image_name, image_class)
        if str(image_class) in roots:
            root = roots[str(image_class)]
        else:
            roots[str(image_class)] = ET.Element("annotations")
            root = roots[str(image_class)]
        image = ET.SubElement(root, 'image', name = image_name)
        content = get_content_from_tasks(item['tasks'])
        ids = list(content.keys()) 
        for id in ids:
            annotations = content[id]['answer']
            for annotation in annotations:
                occluded="0" 
                source="manual"
                z_order="0"
                if 'points' in annotation: # it is a polygon
                    points = annotation['points']
                    label = annotation['description']
                    point_string = points_to_string(points)
                    polygon = ET.SubElement(image, 'polygon', label=label, occluded=occluded, source=source, points=point_string, z_order = z_order)
                else: #it is a bbox
                    xtl = annotation['x']
                    ytl = annotation['y']
                    xbr = xtl + annotation['width']
                    ybr = ytl + annotation['height']
                    box_id =  annotation['id']
                    box_key =  annotation['key']
                    label = annotation['description']
                    polygon = ET.SubElement(image, 'box', label=label, occluded=occluded, source=source, xtl= str(round(xtl, 2)) , ytl=  str(round(ytl, 2)), xbr=str(round(xbr, 2)), ybr=str(round(ybr, 2)), z_order = z_order)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Videos to images')
    parser.add_argument('file_name', type=str, help='Input Json File')
    parser.add_argument('total_items', type=int, help='total Items to be processed from file')
    args = parser.parse_args()
    file_name = args.file_name
    total_items = args.total_items
    roots = {}
    data = read_json(file_name)
    print(f"total data: {len(data)} items")
    #print(f"processing {total_items} items")
    process_data(data, roots, total_items)
    print(roots)
    for key, root in roots.items():
        tree = ET.ElementTree(root)
        tree.write("annotations_" + str(key) + ".xml")