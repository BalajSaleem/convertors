import json
import cv2
import requests
from skimage import io

import os.path

CLASSIFICATION_OF_INTEREST = "Mask"
CLASSIFICATION_OF_INTEREST_INDEX = 1
CLASSIFICATION_VALUES = ['Yes, No']
FILE_NAME = "Annotations.json"
TASK_TYPE = "label" #or review
OBJECT_NAME = "face" 

f = open(FILE_NAME, encoding='utf-8')
data = json.load(f)
f.close()

list1 = []
for x in data['data']:
    asset = x['asset']
    img = io.imread(asset)
    #mg = cv2.cvtColor(img_bgr, cv2.COLOR_RGB2BGR)
    dh, dw, _ = img.shape
    txt_file_name = os.path.splitext(asset)[0]
    #cut_url1 = asset[-15:-4]
    myfile = open('{}.txt'.format(txt_file_name), 'w')
    
    for el in x['tasks']:
        if (el['type']== TASK_TYPE):
            for obj in el['objects']:
                list2 = []
                if (obj['name'] == OBJECT_NAME) and (obj['classifications'][1]['answer'] == CLASSIFICATION_VALUES[0]): #Mask
                    list2.append(0)
                if (obj['name'] == OBJECT_NAME) and (obj['classifications'][1]['answer'] == CLASSIFICATION_VALUES[1]): #NoMask
                    list2.append(1)
                x = obj['bounding-box']['x']
                y = obj['bounding-box']['y']
                w = obj['bounding-box']['width']
                h = obj['bounding-box']['height']
                dh, dw, _ = img.shape
                x_yolo = abs((x + w/2)/dw)
                y_yolo = abs((y + h/2)/dh)
                w_yolo = abs((w)/dw)
                h_yolo = abs((h)/dh)
                list2.extend([x_yolo, y_yolo, w_yolo, h_yolo])
                list1.append(list2)
                myfile.write(' '.join(str(num) for num in list2) + '\n')            
    myfile.close()       
