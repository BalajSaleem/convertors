import json
import cv2
import requests
from skimage import io
import os.path


# Created by Ango AI
# Author: Fatma Oztel
# Reviewer: Balaj saleem
# Yolo Convertor from the Ango format (as was on 16 Aug 2021)
# Currently Bounding Boxes are Supported

CLASSIFICATION_OF_INTEREST = "Mask"
CLASSIFICATION_OF_INTEREST_INDEX = 1
CLASSIFICATION_VALUES = ['Yes','Wrong']
ANNOTATION_FILE_PATH = "../Annotations.json"
IMAGE_FOLDER_PATH = "../Dataset/Images/"
TASK_TYPE = "review" #or review
OBJECT_NAME = "Face" 
IMAGES_ARE_LOCAL = True
MIN_CLASSIFICATION_LEN = 2


def get_yolo_cordinates(obj, img, row):
    x = obj['bounding-box']['x']
    y = obj['bounding-box']['y']
    w = obj['bounding-box']['width']
    h = obj['bounding-box']['height']
    dh, dw, _ = img.shape
    x_yolo = abs((x + w/2)/dw)
    y_yolo = abs((y + h/2)/dh)
    w_yolo = abs((w)/dw)
    h_yolo = abs((h)/dh)
    row.extend([x_yolo, y_yolo, w_yolo, h_yolo])



f = open(ANNOTATION_FILE_PATH, encoding='utf-8')
data = json.load(f)
f.close()

list1 = []
for x in data['data']:
    asset = x['asset']
    print(asset)
    if IMAGES_ARE_LOCAL:
        img = cv2.imread(f'{IMAGE_FOLDER_PATH}{asset}')
    else:
        img = io.imread(asset)
        #mg = cv2.cvtColor(img_bgr, cv2.COLOR_RGB2BGR)
        #cut_url1 = asset[-15:-4]
    dh, dw, _ = img.shape
    txt_file_name = os.path.splitext(asset)[0]
    yolo_out_file = open( '{}.txt'.format(txt_file_name) , 'w')
    for el in x['tasks']:
        if (el['type']== TASK_TYPE):
            for obj in el['objects']:
                list2 = []
                if (obj['name'] == OBJECT_NAME) and len(obj['classifications']) >= MIN_CLASSIFICATION_LEN: 
                    if (obj['classifications'][CLASSIFICATION_OF_INTEREST_INDEX]['answer'] in CLASSIFICATION_VALUES): 
                        list2.append(0) #Mask
                    else:                           #NOTE: Refer to classes.names to determine what value corresponds to what class
                        list2.append(1) #NoMask
                    get_yolo_cordinates(obj, img, list2)
                    list1.append(list2)
                    yolo_out_file.write(' '.join(str(num) for num in list2) + '\n')            
    yolo_out_file.close()       
