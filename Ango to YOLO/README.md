This repository contains the convertors needed to get common formats such as Yolo, COCO, and CVAT from the Ango Export Format.

###YOLO
run: pip install -r requirements.txt in your shell.
modify the yolofy.py file in the following manner:
CLASSIFICATION_OF_INTEREST: The name of classification required for your project this is in data[tasks][objects][classifcations][index][CLASSIFICATION_OF_INTEREST]
CLASSIFICATION_OF_INTEREST_INDEX = The index where your classification is present data[tasks][objects][classifcations][CLASSIFICATION_OF_INTEREST_INDEX][name]
CLASSIFICATION_VALUES = The list of values that can be accepted for the positive class
ANNOTATION_FILE_PATH = The file path to the Ango Format Annotations
IMAGE_FOLDER_PATH = The file path to the Image Folder (the images which were annotated)
TASK_TYPE = "review" The type of annotation that is to be picked for yolo (review or label)
OBJECT_NAME = The object name that is to be extracted - this will later be classified using the classifications to put in different classes
IMAGES_ARE_LOCAL = Whether the images are local or hosted on cloud
MIN_CLASSIFICATION_LEN = The minimum lenght of answers for the classfications.
